package ru.t1.avfilippov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.request.TaskListRequest;
import ru.t1.avfilippov.tm.dto.response.TaskListResponse;
import ru.t1.avfilippov.tm.enumerated.ManualSort;
import ru.t1.avfilippov.tm.event.ConsoleEvent;
import ru.t1.avfilippov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collections;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getDescription() {
        return "display all tasks";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    @EventListener(condition = "@taskListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW TASKS]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(ManualSort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        request.setSort(sortType);
        @Nullable final TaskListResponse response = taskEndpoint.listTasks(request);
        if (response.getTasks() == null) response.setTasks(Collections.emptyList());
        renderTasks(response.getTasks());
    }

}
