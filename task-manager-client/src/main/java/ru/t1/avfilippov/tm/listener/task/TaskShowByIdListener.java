package ru.t1.avfilippov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.request.TaskShowByIdRequest;
import ru.t1.avfilippov.tm.dto.response.TaskShowByIdResponse;
import ru.t1.avfilippov.tm.event.ConsoleEvent;
import ru.t1.avfilippov.tm.exception.entity.TaskNotFoundException;
import ru.t1.avfilippov.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getDescription() {
        return "show task by id";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    @EventListener(condition = "@taskShowByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken());
        request.setTaskId(id);
        @Nullable TaskShowByIdResponse response = taskEndpoint.showTaskById(request);
        if (response.getTask() == null) throw new TaskNotFoundException();
        showTask(response.getTask());
    }

}
