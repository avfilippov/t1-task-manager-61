package ru.t1.avfilippov.tm.api.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.dto.request.UserLoginRequest;
import ru.t1.avfilippov.tm.dto.request.UserLogoutRequest;
import ru.t1.avfilippov.tm.dto.request.UserProfileRequest;
import ru.t1.avfilippov.tm.dto.response.UserLoginResponse;
import ru.t1.avfilippov.tm.dto.response.UserLogoutResponse;
import ru.t1.avfilippov.tm.dto.response.UserProfileResponse;

public interface IAuthEndpointClient {

    @NotNull
    @SneakyThrows
    UserLoginResponse login(@NotNull UserLoginRequest request) throws Exception;

    @NotNull
    @SneakyThrows
    UserLogoutResponse logout(@NotNull UserLogoutRequest request) throws Exception;

    @NotNull
    @SneakyThrows
    UserProfileResponse profile(@NotNull UserProfileRequest request) throws Exception;

}
