package ru.t1.avfilippov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.request.ProjectRemoveByIdRequest;
import ru.t1.avfilippov.tm.event.ConsoleEvent;
import ru.t1.avfilippov.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String getDescription() {
        return "remove project by id";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken());
        request.setProjectId(id);
        projectEndpoint.removeProjectById(request);
    }

}
