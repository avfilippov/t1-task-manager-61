package ru.t1.avfilippov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class ProjectChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private String statusValue;

    public ProjectChangeStatusByIdRequest(@Nullable final String token) {
        super(token);
    }

}
