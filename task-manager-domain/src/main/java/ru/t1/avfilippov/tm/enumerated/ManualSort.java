package ru.t1.avfilippov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.comparator.CreatedComparator;
import ru.t1.avfilippov.tm.comparator.NameComparator;
import ru.t1.avfilippov.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum ManualSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    @Getter
    private final String displayName;

    private final Comparator<?> comparator;

    @Nullable
    public static ManualSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final ManualSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    ManualSort(@NotNull final String displayName, @NotNull final Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    @SuppressWarnings("rawtypes")
    public Comparator getComparator() {
        return comparator;
    }

}
