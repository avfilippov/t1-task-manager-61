package ru.t1.avfilippov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.model.UserDTO;
import ru.t1.avfilippov.tm.enumerated.Role;

public interface IUserDTOService extends IDTOService<UserDTO> {

    @NotNull
    UserDTO create(@Nullable final String login, @Nullable final String password);

    @NotNull
    UserDTO create(@Nullable final String login,
                   @Nullable final String password,
                   @Nullable final String email);

    @NotNull
    UserDTO create(@Nullable final String login,
                   @Nullable final String password,
                   @Nullable final Role role);

    @NotNull
    UserDTO create(@Nullable final String login,
                   @Nullable final String password,
                   @Nullable final String email,
                   @Nullable final String lastName,
                   @Nullable final String firstName,
                   @Nullable final String middleName);

    @Nullable
    UserDTO findByEmail(@Nullable final String email);

    @Nullable
    UserDTO findByLogin(@Nullable final String login);

    Boolean isEmailExists(@Nullable final String email);

    Boolean isLoginExists(@Nullable final String login);

    void lockUserByLogin(@Nullable final String login);

    void removeByLogin(@Nullable final String login) throws Exception;

    void setPassword(@Nullable final String id, @Nullable final String password);

    void unlockUserByLogin(@Nullable final String login);

    void updateUser(@Nullable final String id,
                    @Nullable final String firstName,
                    @Nullable final String lastName,
                    @Nullable final String middleName);

}
