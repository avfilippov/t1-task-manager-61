package ru.t1.avfilippov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.model.AbstractUserOwnedModel;
import ru.t1.avfilippov.tm.enumerated.ManualSort;

import java.util.List;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModel> {

    @NotNull
    M add(@Nullable String userId, @NotNull M model) throws Exception;

    void clear(@Nullable String userId) throws Exception;

    @Nullable
    List<M> findAll(@Nullable String userId) throws Exception;

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable ManualSort sort) throws Exception;

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    void remove(@Nullable String userId, @Nullable M model) throws Exception;

    void removeById(@Nullable String userId, @Nullable String id) throws Exception;

    void update(@Nullable String userId, @Nullable M model) throws Exception;

}
