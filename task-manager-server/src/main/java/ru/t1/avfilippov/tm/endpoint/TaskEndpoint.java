package ru.t1.avfilippov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.avfilippov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.avfilippov.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.avfilippov.tm.api.service.dto.ITaskDTOService;
import ru.t1.avfilippov.tm.dto.model.SessionDTO;
import ru.t1.avfilippov.tm.dto.model.TaskDTO;
import ru.t1.avfilippov.tm.dto.request.*;
import ru.t1.avfilippov.tm.dto.response.*;
import ru.t1.avfilippov.tm.enumerated.ManualSort;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.exception.EndpointException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.avfilippov.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @Override
    @NotNull
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String userId = session.getUserId();
        try {
            projectTaskService.bindTaskToProject(userId, projectId, taskId);
            return new TaskBindToProjectResponse();
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String id = request.getTaskId();
        @Nullable final String statusValue = request.getStatusValue();
        @Nullable final Status status = Status.toStatus(statusValue);
        @Nullable final String userId = session.getUserId();
        try {
            taskService.changeTaskStatusById(userId, id, status);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskChangeStatusByIdResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        try {
            taskService.clear(userId);
            return new TaskClearResponse();
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String id = request.getTaskId();
        @Nullable final String userId = session.getUserId();
        try {
            taskService.changeTaskStatusById(userId, id, Status.COMPLETED);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskCompleteByIdResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = session.getUserId();
        @Nullable final TaskDTO task = taskService.create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskListResponse listTasks(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String sortType = request.getSort();
        @Nullable final ManualSort sort = ManualSort.toSort(sortType);
        @Nullable final String userId = session.getUserId();
        try {
            @NotNull final List<TaskDTO> tasks = taskService.findAll(userId, sort);
            return new TaskListResponse(tasks);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String id = request.getTaskId();
        @Nullable final String userId = session.getUserId();
        try {
            taskService.removeById(userId, id);
            return new TaskRemoveByIdResponse();
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }

    }

    @Override
    @NotNull
    @WebMethod
    public TaskShowByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String id = request.getTaskId();
        @Nullable final String userId = session.getUserId();
        try {
            @Nullable final TaskDTO task = taskService.findOneById(userId, id);
            return new TaskShowByIdResponse(task);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskShowByProjectIdResponse showTaskByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByProjectIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String userId = session.getUserId();
        @NotNull final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        return new TaskShowByProjectIdResponse(tasks);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String id = request.getTaskId();
        @Nullable final String userId = session.getUserId();
        try {
            taskService.changeTaskStatusById(userId, id, Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskStartByIdResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String userId = session.getUserId();
        try {
            projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
            return new TaskUnbindFromProjectResponse();
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String id = request.getTaskId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = session.getUserId();
        try {
            taskService.updateById(userId, id, name, description);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskUpdateByIdResponse();
    }

}
