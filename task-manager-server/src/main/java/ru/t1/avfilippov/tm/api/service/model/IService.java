package ru.t1.avfilippov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.enumerated.ManualSort;
import ru.t1.avfilippov.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> {


    @NotNull
    M add(@NotNull M model) throws Exception;

    void clear() throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @Nullable
    List<M> findAll() throws Exception;

    @Nullable
    List<M> findAll(@Nullable ManualSort sort) throws Exception;

    @Nullable
    M findOneById(@Nullable String id) throws Exception;

    int getSize() throws Exception;

    void remove(@Nullable M model) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    void update(@Nullable M model) throws Exception;

}
