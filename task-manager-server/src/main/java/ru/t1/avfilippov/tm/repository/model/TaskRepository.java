package ru.t1.avfilippov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.avfilippov.tm.model.Task;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    long countByUserId(@NotNull final String userId);

    void deleteByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Query("select case when count(t)> 0 then true else false end from Task t where t.user.id = :userId and t.id = :id")
    boolean existByUserIdAndId(@Param("userId") String userId, @Param("id") String id);

    @NotNull
    List<Task> findByUserId(@NotNull final String userId);

    @NotNull
    List<Task> findByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @NotNull
    Optional<Task> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    List<Task> findByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

}
