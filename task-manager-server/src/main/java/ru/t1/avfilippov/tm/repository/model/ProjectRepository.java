package ru.t1.avfilippov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.avfilippov.tm.dto.model.ProjectDTO;
import ru.t1.avfilippov.tm.model.Project;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

    long countByUserId(@NotNull final String userId);

    void deleteByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Query("select case when count(p)> 0 then true else false end from Project p where p.user.id = :userId and p.id = :id")
    boolean existByUserIdAndId(@Param("userId") String userId, @Param("id") String id);

    @NotNull
    List<Project> findByUserId(@NotNull final String userId);

    @NotNull
    List<Project> findByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @NotNull
    Optional<Project> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
