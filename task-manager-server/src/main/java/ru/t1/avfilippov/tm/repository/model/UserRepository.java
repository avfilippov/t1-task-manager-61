package ru.t1.avfilippov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.avfilippov.tm.model.User;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    @NotNull
    User findByEmail(@NotNull final String email);

    @NotNull
    User findByLogin(@NotNull final String login);

    @Query("select case when count(u)> 0 then true else false end from User u where u.email = :email")
    boolean isEmailExists(@Param("email") String email);

    @Query("select case when count(u)> 0 then true else false end from User u where u.login = :login")
    boolean isLoginExists(@Param("login") String login);

}
