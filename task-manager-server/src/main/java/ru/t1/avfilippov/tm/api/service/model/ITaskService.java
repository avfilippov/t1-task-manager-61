package ru.t1.avfilippov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    void changeTaskStatusById(@Nullable String userId,
                              @Nullable String id,
                              @Nullable Status status) throws Exception;

    @NotNull
    Task create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    Task create(@Nullable String userId,
                @Nullable String name,
                @Nullable String description) throws Exception;

    @Nullable
    List<Task> findAllByProjectId(@Nullable String userId,
                                  @Nullable String projectId);

    void updateById(@Nullable String userId,
                    @Nullable String id,
                    @Nullable String name,
                    @Nullable String description) throws Exception;

    void updateProjectIdById(@Nullable final String userId,
                             @Nullable final String id,
                             @Nullable final String projectId) throws Exception;

}
