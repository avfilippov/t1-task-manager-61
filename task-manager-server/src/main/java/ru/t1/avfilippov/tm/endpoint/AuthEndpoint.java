package ru.t1.avfilippov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.avfilippov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.avfilippov.tm.api.service.IAuthService;
import ru.t1.avfilippov.tm.api.service.dto.IUserDTOService;
import ru.t1.avfilippov.tm.dto.model.SessionDTO;
import ru.t1.avfilippov.tm.dto.model.UserDTO;
import ru.t1.avfilippov.tm.dto.request.UserLoginRequest;
import ru.t1.avfilippov.tm.dto.request.UserLogoutRequest;
import ru.t1.avfilippov.tm.dto.request.UserProfileRequest;
import ru.t1.avfilippov.tm.dto.response.UserLoginResponse;
import ru.t1.avfilippov.tm.dto.response.UserLogoutResponse;
import ru.t1.avfilippov.tm.dto.response.UserProfileResponse;
import ru.t1.avfilippov.tm.exception.EndpointException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.avfilippov.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @NotNull
    @Autowired
    private IUserDTOService userDTOService;

    @NotNull
    @Autowired
    private IAuthService authService;

    @Override
    @NotNull
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @Override
    @NotNull
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        try {
            authService.invalidate(session);
            return new UserLogoutResponse();
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }

    }

    @Override
    @NotNull
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserProfileRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        try {
            @Nullable final UserDTO user = userDTOService.findOneById(userId);
            return new UserProfileResponse(user);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

}
