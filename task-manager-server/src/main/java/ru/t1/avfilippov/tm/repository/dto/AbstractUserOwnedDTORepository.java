package ru.t1.avfilippov.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.avfilippov.tm.dto.model.AbstractUserOwnedModel;

@Repository
@Scope("prototype")
public interface AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModel>
        extends AbstractDTORepository<M> {

}
