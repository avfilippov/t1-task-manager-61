package ru.t1.avfilippov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.avfilippov.tm.api.service.dto.IDTOService;
import ru.t1.avfilippov.tm.dto.model.AbstractModelDTO;
import ru.t1.avfilippov.tm.enumerated.ManualSort;
import ru.t1.avfilippov.tm.exception.field.IdEmptyException;
import ru.t1.avfilippov.tm.repository.dto.AbstractDTORepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO> implements IDTOService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Autowired
    private AbstractDTORepository<M> repository;


    @NotNull
    @Override
    @Transactional
    public M add(@NotNull final M model) {
        return repository.save(model);
    }

    @Override
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public @Nullable List<M> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<M> result = repository.findById(id);
        return result.orElse(null);
    }

    @Override
    public int getSize() {
        return (int) repository.count();
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        if (model == null) return;
        repository.delete(model);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        if (model == null) return;
        repository.save(model);
    }

    @Override
    @Nullable
    public List<M> findAll(@Nullable final ManualSort sort) {
        if (sort == null) return findAll();
        @Nullable final Sort findSort;
        switch (sort.name()) {
            case "BY_NAME":
                findSort = Sort.by(Sort.Direction.ASC, "name");
                break;
            case "BY_STATUS":
                findSort = Sort.by(Sort.Direction.ASC, "status");
                break;
            default:
                findSort = Sort.by(Sort.Direction.ASC, "created");
        }
        return repository.findAll(findSort);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        @NotNull M model = findOneById(id);
        remove(model);
    }

}
