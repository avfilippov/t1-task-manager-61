package ru.t1.avfilippov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    void changeProjectStatusById(@Nullable String userId,
                                 @Nullable String id,
                                 @Nullable Status status) throws Exception;

    @NotNull
    Project create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    Project create(@Nullable String userId,
                   @Nullable String name,
                   @Nullable String description) throws Exception;

    void updateById(@Nullable String userId,
                    @Nullable String id,
                    @Nullable String name,
                    @Nullable String description) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

}
