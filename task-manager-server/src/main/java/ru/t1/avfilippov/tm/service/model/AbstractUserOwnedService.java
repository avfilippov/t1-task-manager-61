package ru.t1.avfilippov.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.avfilippov.tm.api.service.model.IUserOwnedService;
import ru.t1.avfilippov.tm.api.service.model.IUserService;
import ru.t1.avfilippov.tm.exception.field.IdEmptyException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.model.AbstractUserOwnedModel;
import ru.t1.avfilippov.tm.repository.model.AbstractUserOwnedRepository;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel>
        extends AbstractService<M> implements IUserOwnedService<M> {

    @NotNull
    @Autowired
    private AbstractUserOwnedRepository<M> repository;

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    @NotNull
    @Transactional
    public M add(@NotNull final String userId, @NotNull final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        model.setUser(userService.findOneById(userId));
        return repository.save(model);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.findAll().stream()
                .filter(x -> x.getUser().getId().equals(userId))
                .forEach(x -> repository.delete(x));
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @NotNull final M model) throws Exception {
        if (model == null) return;
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (findOneById(model.getUser().getId(), model.getId()) == null) return;
        repository.delete(model);
    }

    @Override
    @Transactional
    public void update(@NotNull final String userId, @NotNull final M model) throws Exception {
        if (model == null) return;
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (findOneById(model.getUser().getId(), model.getId()) == null) return;
        model.setUser(userService.findOneById(userId));
        repository.save(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M model = findOneById(userId, id);
        if (model != null) remove(userId, model);
    }

}
