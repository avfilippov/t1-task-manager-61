package ru.t1.avfilippov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.model.TaskDTO;
import ru.t1.avfilippov.tm.enumerated.ManualSort;
import ru.t1.avfilippov.tm.enumerated.Status;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @Nullable
    @SneakyThrows
    List<TaskDTO> findAll(@Nullable String userId);

    @Nullable
    @SneakyThrows
    List<TaskDTO> findAll(
            @Nullable String userId,
            @Nullable Comparator comparator
    );

    @Nullable
    @SneakyThrows
    List<TaskDTO> findAll(
            @Nullable String userId,
            @Nullable ManualSort sort
    );

    @NotNull
    @SneakyThrows
    TaskDTO add(@Nullable String userId, @Nullable TaskDTO model);

    @NotNull
    @SneakyThrows
    Collection<TaskDTO> add(@NotNull Collection<TaskDTO> models);

    @NotNull
    @SneakyThrows
    Collection<TaskDTO> set(@NotNull Collection<TaskDTO> models);

    @SneakyThrows
    void clear(@Nullable String userId);

    @SneakyThrows
    boolean existsById(@Nullable String userId, @Nullable String id);

    @NotNull
    @SneakyThrows
    TaskDTO remove(@NotNull String userId, @Nullable TaskDTO model);

    @Nullable
    @SneakyThrows
    TaskDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    TaskDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    @SneakyThrows
    TaskDTO removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    @SneakyThrows
    TaskDTO removeByIndex(@Nullable String userId, @Nullable Integer index);

    @SneakyThrows
    void removeAll(@Nullable String userId);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    TaskDTO changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}
