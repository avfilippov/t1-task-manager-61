package ru.t1.avfilippov.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.avfilippov.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.avfilippov.tm.dto.model.AbstractUserOwnedModel;
import ru.t1.avfilippov.tm.exception.field.IdEmptyException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.repository.dto.AbstractUserOwnedDTORepository;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModel>
        extends AbstractDTOService<M> implements IUserOwnedDTOService<M> {

    @NotNull
    @Autowired
    private AbstractUserOwnedDTORepository<M> repository;

    @Override
    @NotNull
    @Transactional
    public M add(@NotNull final String userId, @NotNull final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        model.setUserId(userId);
        return repository.save(model);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.findAll().stream()
                .filter(x -> x.getUserId().equals(userId))
                .forEach(x -> repository.delete(x));
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @NotNull final M model) throws Exception {
        if (model == null) return;
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (findOneById(model.getUserId(), model.getId()) == null) return;
        repository.delete(model);
    }

    @Override
    @Transactional
    public void update(@NotNull final String userId, @NotNull final M model) throws Exception {
        if (model == null) return;
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (findOneById(model.getUserId(), model.getId()) == null) return;
        model.setUserId(userId);
        repository.save(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M model = findOneById(userId, id);
        if (model != null) remove(userId, model);
    }

}
