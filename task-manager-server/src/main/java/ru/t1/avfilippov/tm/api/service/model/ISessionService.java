package ru.t1.avfilippov.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;


}
