package ru.t1.avfilippov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.api.service.ILoggerService;
import ru.t1.avfilippov.tm.endpoint.AbstractEndpoint;
import ru.t1.avfilippov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Component
@NoArgsConstructor
public final class Bootstrap {

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    private void initEndpoints(@NotNull final AbstractEndpoint[] endpoints) {
        for (@NotNull AbstractEndpoint endpoint : endpoints) {
            registry(endpoint);
        }
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = "0.0.0.0";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }


    public void start() {
        initPID();
        initEndpoints(endpoints);
        loggerService.initJmsLogger();
        loggerService.info("*** TASK MANAGER SERVER STARTED ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    private void stop() {
        loggerService.info("*** TASK MANAGER SERVER IS SHUTTING DOWN***");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
