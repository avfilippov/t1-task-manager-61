package ru.t1.avfilippov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.model.TaskDTO;
import ru.t1.avfilippov.tm.enumerated.Status;

import java.util.List;

public interface ITaskDTOService extends IUserOwnedDTOService<TaskDTO> {

    void changeTaskStatusById(@Nullable String userId,
                              @Nullable String id,
                              @Nullable Status status) throws Exception;

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO create(@Nullable String userId,
                   @Nullable String name,
                   @Nullable String description);

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable String userId,
                                     @Nullable String projectId);

    void updateById(@Nullable String userId,
                    @Nullable String id,
                    @Nullable String name,
                    @Nullable String description) throws Exception;

    void updateProjectIdById(@Nullable final String userId,
                             @Nullable final String id,
                             @Nullable final String projectId) throws Exception;

}
