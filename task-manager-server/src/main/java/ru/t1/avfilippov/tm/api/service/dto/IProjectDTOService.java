package ru.t1.avfilippov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.model.ProjectDTO;
import ru.t1.avfilippov.tm.enumerated.Status;

import java.util.List;

public interface IProjectDTOService extends IUserOwnedDTOService<ProjectDTO> {

    void changeProjectStatusById(@Nullable String userId,
                                 @Nullable String id,
                                 @Nullable Status status) throws Exception;

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO create(@Nullable String userId,
                      @Nullable String name,
                      @Nullable String description);

    void updateById(@Nullable String userId,
                    @Nullable String id,
                    @Nullable String name,
                    @Nullable String description) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

}
