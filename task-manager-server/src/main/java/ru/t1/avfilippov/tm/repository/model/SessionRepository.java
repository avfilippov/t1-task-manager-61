package ru.t1.avfilippov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.avfilippov.tm.model.Session;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface SessionRepository extends AbstractUserOwnedRepository<Session> {
    long countByUserId(@NotNull final String userId);

    void deleteByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Query("select case when count(s)> 0 then true else false end from Session s where s.user.id = :userId and s.id = :id")
    boolean existByUserIdAndId(@Param("userId") String userId, @Param("id") String id);

    @NotNull
    List<Session> findByUserId(@NotNull final String userId);

    @NotNull
    List<Session> findByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @NotNull
    Optional<Session> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}

