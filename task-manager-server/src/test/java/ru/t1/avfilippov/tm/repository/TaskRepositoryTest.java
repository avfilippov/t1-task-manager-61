package ru.t1.avfilippov.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.api.service.IPropertyService;
import ru.t1.avfilippov.tm.api.service.dto.IProjectDTOService;
import ru.t1.avfilippov.tm.api.service.dto.ITaskDTOService;
import ru.t1.avfilippov.tm.api.service.dto.IUserDTOService;
import ru.t1.avfilippov.tm.comparator.NameComparator;
import ru.t1.avfilippov.tm.dto.model.TaskDTO;
import ru.t1.avfilippov.tm.dto.model.UserDTO;
import ru.t1.avfilippov.tm.marker.DataCategory;
import ru.t1.avfilippov.tm.migration.AbstractSchemeTest;
import ru.t1.avfilippov.tm.repository.dto.TaskDTORepository;
import ru.t1.avfilippov.tm.service.PropertyService;
import ru.t1.avfilippov.tm.service.dto.ProjectDTOService;
import ru.t1.avfilippov.tm.service.dto.TaskDTOService;
import ru.t1.avfilippov.tm.service.dto.UserDTOService;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static ru.t1.avfilippov.tm.constant.ProjectTestData.USER_PROJECT1;
import static ru.t1.avfilippov.tm.constant.TaskTestData.*;
import static ru.t1.avfilippov.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.t1.avfilippov.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(DataCategory.class)
public final class TaskRepositoryTest extends AbstractSchemeTest {
/*
    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private static final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService( connectionService, projectService, taskService,propertyService);

    @NotNull
    private static String userId = "";

    @NotNull
    private static ITaskDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskDTORepository(entityManager);
    }

    @NotNull
    private static EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @BeforeClass
    public static void setUp() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        PropertyService propertyService = new PropertyService();
        ConnectionService connectionService = new ConnectionService(propertyService);

        @NotNull final UserDTO user = userService.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
        projectService.add(userId, USER_PROJECT1);
    }

    @AfterClass
    public static void tearDown() {
        @Nullable final UserDTO user = userService.findByLogin(USER_TEST_LOGIN);
        projectService.remove(userId, USER_PROJECT1);
        if (user != null) userService.remove(user);
        connectionService.close();
    }

    @Before
    public void initTest() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, USER_TASK1);
            repository.add(userId, USER_TASK2);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void clean() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void addByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNotNull(repository.add(userId, USER_TASK3));
            entityManager.getTransaction().commit();
            @Nullable final TaskDTO task = repository.findOneById(userId, USER_TASK3.getId());
            Assert.assertNotNull(task);
            Assert.assertEquals(USER_TASK3.getId(), task.getId());
            Assert.assertEquals(userId, task.getUserId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final TaskDTO task = repository.create(userId, USER_TASK3.getName());
            Assert.assertEquals(USER_TASK3.getName(), task.getName());
            Assert.assertEquals(userId, task.getUserId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserIdWithDescription() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final TaskDTO task = repository.create(userId, USER_TASK3.getName(), USER_TASK3.getDescription());
            Assert.assertEquals(USER_TASK3.getName(), task.getName());
            Assert.assertEquals(USER_TASK3.getDescription(), task.getDescription());
            Assert.assertEquals(userId, task.getUserId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAllByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<TaskDTO> tasks = repository.findAll(userId);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
        entityManager.close();
    }

    @Test
    public void findAllComparatorByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<TaskDTO> tasks = repository.findAll(userId, comparator);
        Assert.assertNotNull(tasks);
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
        entityManager.close();
    }

    @Test
    public void existsByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        Assert.assertFalse(repository.existsById(userId, NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(userId, USER_TASK1.getId()));
        entityManager.close();
    }

    @Test
    public void findOneByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        Assert.assertNull(repository.findOneById(userId, NON_EXISTING_TASK_ID));
        @Nullable final TaskDTO task = repository.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
        entityManager.close();
    }

    @Test
    public void clearByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
            Assert.assertEquals(0, repository.getSize(userId));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(userId, USER_TASK2);
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findOneById(USER_TASK2.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void getSizeByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(2, repository.getSize(userId));
        entityManager.close();
    }

    @Test
    public void update() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            USER_TASK1.setName(USER_TASK3.getName());
            repository.update(USER_TASK1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER_TASK3.getName(), repository.findOneById(userId, USER_TASK1.getId()).getName());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
*/
}
