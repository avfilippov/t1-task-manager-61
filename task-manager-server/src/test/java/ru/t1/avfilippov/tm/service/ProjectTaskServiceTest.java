package ru.t1.avfilippov.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.api.service.IPropertyService;
import ru.t1.avfilippov.tm.api.service.dto.IProjectDTOService;
import ru.t1.avfilippov.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.avfilippov.tm.api.service.dto.ITaskDTOService;
import ru.t1.avfilippov.tm.api.service.dto.IUserDTOService;
import ru.t1.avfilippov.tm.dto.model.TaskDTO;
import ru.t1.avfilippov.tm.dto.model.UserDTO;
import ru.t1.avfilippov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.avfilippov.tm.exception.entity.TaskNotFoundException;
import ru.t1.avfilippov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.avfilippov.tm.exception.field.TaskIdEmptyException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.marker.UnitCategory;
import ru.t1.avfilippov.tm.migration.AbstractSchemeTest;
import ru.t1.avfilippov.tm.service.dto.ProjectDTOService;
import ru.t1.avfilippov.tm.service.dto.ProjectTaskDTOService;
import ru.t1.avfilippov.tm.service.dto.TaskDTOService;
import ru.t1.avfilippov.tm.service.dto.UserDTOService;

import static ru.t1.avfilippov.tm.constant.ProjectTestData.*;
import static ru.t1.avfilippov.tm.constant.TaskTestData.*;
import static ru.t1.avfilippov.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.t1.avfilippov.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest extends AbstractSchemeTest {
/*
    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private static final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(connectionService, projectService, taskService, propertyService);

    @NotNull
    private static String userId = "";

    @NotNull
    private final IProjectTaskDTOService service = new ProjectTaskDTOService(projectService, taskService);

    @BeforeClass
    public static void setUp() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        PropertyService propertyService = new PropertyService();
        ConnectionService connectionService = new ConnectionService(propertyService);

        @NotNull final UserDTO user = userService.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        connectionService.close();
    }

    @Before
    public void initTest() throws Exception {
        projectService.add(userId, USER_PROJECT1);
        projectService.add(userId, USER_PROJECT2);
        taskService.add(userId, USER_TASK1);
        taskService.add(userId, USER_TASK2);
    }

    @After
    public void clean() throws Exception {
        taskService.clear(userId);
        projectService.clear(userId);
    }

    @Test
    public void bindTaskToProject() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.bindTaskToProject(null, USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.bindTaskToProject("", USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.bindTaskToProject(userId, null, USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.bindTaskToProject(userId, "", USER_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.bindTaskToProject(userId, USER_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.bindTaskToProject(userId, USER_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.bindTaskToProject(userId, NON_EXISTING_PROJECT_ID, USER_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.bindTaskToProject(userId, USER_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        service.bindTaskToProject(userId, USER_PROJECT2.getId(), USER_TASK1.getId());
        @Nullable final TaskDTO task = taskService.findOneById(userId, USER_TASK1.getId());
        Assert.assertEquals(USER_PROJECT2.getId(), task.getProjectId());
        service.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
    }

    @Test
    public void removeProjectById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeProjectById(null, USER_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeProjectById("", USER_PROJECT1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.removeProjectById(userId, null);
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.removeProjectById(userId, "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.removeProjectById(userId, NON_EXISTING_PROJECT_ID);
        });
        service.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
        service.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK2.getId());
        service.removeProjectById(userId, USER_PROJECT1.getId());
        Assert.assertNull(projectService.findOneById(userId, USER_PROJECT1.getId()));
        Assert.assertNull(taskService.findOneById(userId, USER_TASK1.getId()));
        Assert.assertNull(taskService.findOneById(userId, USER_TASK2.getId()));
    }

    @Test
    public void unbindTaskFromProject() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.unbindTaskFromProject(null, USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.unbindTaskFromProject("", USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.unbindTaskFromProject(userId, null, USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.unbindTaskFromProject(userId, "", USER_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.unbindTaskFromProject(userId, USER_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.unbindTaskFromProject(userId, USER_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.unbindTaskFromProject(userId, NON_EXISTING_PROJECT_ID, USER_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.unbindTaskFromProject(userId, USER_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        service.unbindTaskFromProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
        @Nullable final TaskDTO task = taskService.findOneById(userId, USER_TASK1.getId());
        Assert.assertNull(task.getProjectId());
        service.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
    }
*/
}
